package ejercicios;

import java.util.Scanner;

public class Ej2 {
	
	public static void main(String[] args){
		/*Establecer un breakpoint en la primera instrucci�n y avanzar
		 * instrucci�n a instrucci�n (step into) analizando el contenido de las variables
		 */
		
		Scanner input ;
		int numeroLeido;
		String cadenaLeida;		
		
		input = new Scanner(System.in);
		
		System.out.println("Introduce un numero ");
		numeroLeido = input.nextInt();
		
		System.out.println("Introduce un numero como String");
		cadenaLeida = input.nextLine();
		
		if ( numeroLeido == Integer.parseInt(cadenaLeida)){
			System.out.println("Lo datos introducidos reprensentan el mismo n�mero");
		}
		
		input.close();
	
	}}
/*
 * no se puede hacer sin haber limpiado/borrad el Buffer
 * solucion: a�adir input.nextLine();
 */

