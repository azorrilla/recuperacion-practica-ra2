package ejercicios;

import java.util.Scanner;

public class Ej4 {
	
	public static void main(String[] args) {
		/*Establecer un breakpoint en la primera instrucci�n y avanzar
		 * instrucci�n a instrucci�n (step into) analizando el contenido de las variables
		 */
		
		Scanner lector;
		int numeroLeido;
		int resultadoDivision;
		
		lector = new Scanner(System.in);
		System.out.println("Introduce un numero");
		numeroLeido = lector.nextInt();

		for(int i = numeroLeido; i >= 0 ; i--){
			resultadoDivision = numeroLeido / i;
			System.out.println("el resultado de la division es: " + resultadoDivision);
		}
		
		
		lector.close();
	}



}


/*
 * imposible dividir para 0 al ser mayor que 1
 * una posible soluci�n ser�a cambiar la variable 1>= 0 para que sea mayor o igual que 0 y podr�a dividir para 0
 */

