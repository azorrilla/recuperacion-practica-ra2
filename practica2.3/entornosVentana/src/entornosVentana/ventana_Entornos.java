package entornosVentana;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JToolBar;
import javax.swing.JButton;
import javax.swing.JTabbedPane;
import java.awt.FlowLayout;
import javax.swing.JTextField;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.JRadioButton;
import java.awt.SystemColor;
import java.awt.Toolkit;
import javax.swing.ButtonGroup;
import javax.swing.JTextPane;
import javax.swing.JScrollBar;
import javax.swing.ImageIcon;
import javax.swing.JTextArea;
import java.awt.Color;
import javax.swing.JComboBox;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ventana_Entornos extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_6;
	private JTextField textField_7;
	private JTextField textField_8;
	private JTextField textField_9;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JTextField textField_10;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ventana_Entornos frame = new ventana_Entornos();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ventana_Entornos() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\WIFI\\Desktop\\pregunta.png"));
		setTitle("VENTANA PRACTICA ENTORNOS DE DESARROLLO");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 554, 230);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnArchivo = new JMenu("Archivo");
		menuBar.add(mnArchivo);
		
		JMenuItem mntmEditar = new JMenuItem("Editar");
		mnArchivo.add(mntmEditar);
		
		JMenuItem mntmEliminar = new JMenuItem("Eliminar");
		mnArchivo.add(mntmEliminar);
		
		JMenu mnAyuda = new JMenu("Ayuda");
		menuBar.add(mnAyuda);
		
		JMenuItem mntmContacto = new JMenuItem("Contacto");
		mnAyuda.add(mntmContacto);
		
		JMenuItem mntmDonaciones = new JMenuItem("Donaciones");
		mnAyuda.add(mntmDonaciones);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(10, 11, 520, 153);
		contentPane.add(tabbedPane);
		
		JPanel panel_1 = new JPanel();
		panel_1.setForeground(SystemColor.activeCaption);
		tabbedPane.addTab("Series", new ImageIcon("C:\\Users\\WIFI\\Downloads\\cancelar(1).png"), panel_1, null);
		panel_1.setLayout(null);
		
		textField_3 = new JTextField();
		textField_3.setBounds(10, 11, 86, 20);
		panel_1.add(textField_3);
		textField_3.setColumns(10);
		
		JButton btnNewButton_5 = new JButton("Nombre");
		btnNewButton_5.setBounds(106, 10, 89, 23);
		panel_1.add(btnNewButton_5);
		
		JButton btnNewButton_6 = new JButton("Protagonista");
		btnNewButton_6.setBounds(106, 44, 89, 23);
		panel_1.add(btnNewButton_6);
		
		textField_4 = new JTextField();
		textField_4.setBounds(10, 45, 86, 20);
		panel_1.add(textField_4);
		textField_4.setColumns(10);
		
		JButton btnNewButton_7 = new JButton("Fecha");
		btnNewButton_7.setBounds(106, 78, 89, 23);
		panel_1.add(btnNewButton_7);
		
		textField_5 = new JTextField();
		textField_5.setBounds(10, 79, 86, 20);
		panel_1.add(textField_5);
		textField_5.setColumns(10);
		
		JButton btnNewButton_8 = new JButton("Premios");
		btnNewButton_8.setBounds(326, 10, 89, 23);
		panel_1.add(btnNewButton_8);
		
		textField_6 = new JTextField();
		textField_6.setBounds(230, 11, 86, 20);
		panel_1.add(textField_6);
		textField_6.setColumns(10);
		
		JButton btnNewButton_9 = new JButton("Puntuacion");
		btnNewButton_9.setBounds(230, 44, 89, 23);
		panel_1.add(btnNewButton_9);
		
		JSpinner spinner_1 = new JSpinner();
		spinner_1.setModel(new SpinnerNumberModel(5, 0, 10, 1));
		spinner_1.setBounds(336, 45, 29, 20);
		panel_1.add(spinner_1);
		
		JButton btnEnviarSerieA = new JButton("Enviar serie a la base de datos");
		btnEnviarSerieA.setForeground(SystemColor.activeCaption);
		btnEnviarSerieA.setBounds(240, 78, 244, 23);
		panel_1.add(btnEnviarSerieA);
		
		JButton btnDescargar = new JButton("Descargar");
		btnDescargar.setForeground(SystemColor.desktop);
		btnDescargar.setBounds(395, 44, 110, 23);
		panel_1.add(btnDescargar);
		
		JPanel panel_2 = new JPanel();
		panel_2.setForeground(SystemColor.activeCaption);
		tabbedPane.addTab("Peliculas", new ImageIcon("C:\\Users\\WIFI\\Desktop\\boton-de-reproduccion.png"), panel_2, null);
		panel_2.setLayout(null);
		
		textField_7 = new JTextField();
		textField_7.setBounds(10, 11, 86, 20);
		panel_2.add(textField_7);
		textField_7.setColumns(10);
		
		JButton btnNewButton_10 = new JButton("Director");
		btnNewButton_10.setBounds(106, 10, 89, 23);
		panel_2.add(btnNewButton_10);
		
		JButton btnNewButton_11 = new JButton("N\u00BA personas");
		btnNewButton_11.setBounds(106, 44, 89, 23);
		panel_2.add(btnNewButton_11);
		
		textField_8 = new JTextField();
		textField_8.setBounds(10, 45, 86, 20);
		panel_2.add(textField_8);
		textField_8.setColumns(10);
		
		textField_9 = new JTextField();
		textField_9.setBounds(10, 76, 86, 20);
		panel_2.add(textField_9);
		textField_9.setColumns(10);
		
		JButton btnNewButton_12 = new JButton("Oscar's");
		btnNewButton_12.setBounds(106, 75, 89, 23);
		panel_2.add(btnNewButton_12);
		
		JRadioButton rdbtnLaRecomiendo = new JRadioButton("La recomiendo");
		buttonGroup.add(rdbtnLaRecomiendo);
		rdbtnLaRecomiendo.setBounds(216, 10, 109, 23);
		panel_2.add(rdbtnLaRecomiendo);
		
		JRadioButton rdbtnNoLaRecomiendo = new JRadioButton("No la recomiendo");
		buttonGroup.add(rdbtnNoLaRecomiendo);
		rdbtnNoLaRecomiendo.setBounds(327, 10, 109, 23);
		panel_2.add(rdbtnNoLaRecomiendo);
		
		JButton btnVerOnline = new JButton("VER ONLINE");
		btnVerOnline.setForeground(new Color(0, 204, 0));
		btnVerOnline.setBounds(406, 44, 119, 23);
		panel_2.add(btnVerOnline);
		
		textField_10 = new JTextField();
		textField_10.setBounds(215, 45, 86, 20);
		panel_2.add(textField_10);
		textField_10.setColumns(10);
		
		JButton btnNewButton_13 = new JButton("Nombre");
		btnNewButton_13.setBounds(307, 44, 89, 23);
		panel_2.add(btnNewButton_13);
		
		JButton btnNewButton_14 = new JButton("Enviar pelicula a la base de datos");
		btnNewButton_14.setForeground(SystemColor.activeCaption);
		btnNewButton_14.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton_14.setBounds(263, 75, 205, 23);
		panel_2.add(btnNewButton_14);
		
		JPanel panel = new JPanel();
		panel.setBounds(15, 39, 515, 125);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JButton btnNewButton = new JButton("Protagonista");
		btnNewButton.setBounds(170, 11, 89, 23);
		panel.add(btnNewButton);
		
		textField = new JTextField();
		textField.setBounds(74, 12, 86, 20);
		panel.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(74, 43, 86, 20);
		panel.add(textField_1);
		textField_1.setColumns(10);
		
		JButton btnNewButton_1 = new JButton("Duracion");
		btnNewButton_1.setBounds(170, 42, 89, 23);
		panel.add(btnNewButton_1);
		
		textField_2 = new JTextField();
		textField_2.setBounds(74, 74, 86, 20);
		panel.add(textField_2);
		textField_2.setColumns(10);
		
		JButton btnNewButton_2 = new JButton("Genero");
		btnNewButton_2.setBounds(170, 73, 89, 23);
		panel.add(btnNewButton_2);
		
		JButton btnNewButton_3 = new JButton("Puntuacion");
		btnNewButton_3.setBounds(269, 11, 89, 23);
		panel.add(btnNewButton_3);
		
		JSpinner spinner = new JSpinner();
		spinner.setModel(new SpinnerNumberModel(5, 0, 10, 1));
		spinner.setBounds(368, 12, 29, 20);
		panel.add(spinner);
		
		JButton btnNewButton_4 = new JButton("New button");
		btnNewButton_4.setBounds(335, 42, 89, 23);
		panel.add(btnNewButton_4);
		
		JRadioButton rdbtnNewRadioButton = new JRadioButton("New radio button");
		rdbtnNewRadioButton.setBounds(265, 73, 109, 23);
		panel.add(rdbtnNewRadioButton);
		
		JRadioButton rdbtnNewRadioButton_1 = new JRadioButton("New radio button");
		rdbtnNewRadioButton_1.setBounds(378, 73, 109, 23);
		panel.add(rdbtnNewRadioButton_1);
	}
}
